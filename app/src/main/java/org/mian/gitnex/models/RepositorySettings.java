package org.mian.gitnex.models;

/**
 * Author opyale
 */

public class RepositorySettings {

	private boolean http_git_disabled;
	private boolean mirrors_disabled;

	public boolean isHttp_git_disabled() {

		return http_git_disabled;
	}

	public boolean isMirrors_disabled() {

		return mirrors_disabled;
	}

}
